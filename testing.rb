require './db_model.rb'
require 'sqlite3'

filename = 'output/test.db'

def create_data(filename, steps)
  File::unlink(filename) if File::exists? filename
  model = Db_model.new(filename, ConsoleLogger.new)
  model.create
  model.month = 0
  model.add_members(1)
  steps.times do |i|
    model.month = i + 1
    model.minimal_step()
  end
  model
end

def test_three_steps(filename)
  model = create_data(filename, 3)
  print "MEMBERS:"
  model.print_list()
  print "STATS:"
  model.print_stats()
end

def test_get_tree(filename)
  db = SQLite3::Database.new(filename, {:results_as_hash => true})
  tree = MembersTree.new db
  tree.build(3)
  tree.refresh_stats
  tree.save_stats
  tree.printout
end

test_three_steps(filename)
test_get_tree(filename)
