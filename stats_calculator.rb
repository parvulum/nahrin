#--------------------------------------------------------------------
#  Класс расчёта статусов и премий  
#--------------------------------------------------------------------
class EasyStatusModel
  class Rule
    
    attr_reader :status
     
    def initialize(status, req = {})
      @status = status
      @points = req[:points] || 0
      @group_points = req[:group_points] || 0
      @liders = req[:liders] || 0        
    end
    
    def match(stats)
      stats[:points] >= @points and stats[:group_points_sum] >= @group_points and stats[:liders] >= @liders
    end
  end
    
  def initialize(rules = {})
    if(rules.empty?)
      rules = 
        {
          1  => {:points => 0,   :group_points=>0,    :liders=>0},  #  Консультант  
          2  => {:points => 100, :group_points=>250,  :liders=>0},  #  Старший Консультант
          3  => {:points => 100, :group_points=>500,  :liders=>0},  #  Инструктор
          4  => {:points => 100, :group_points=>750,  :liders=>0},  #  Старший Инструктор
          5  => {:points => 100, :group_points=>1000, :liders=>0},  #  Лидер
          6  => {:points => 100, :group_points=>1000, :liders=>1},  #  Старший Лидер
          7  => {:points => 100, :group_points=>1000, :liders=>3},  #  Главный Лидер
          8  => {:points => 100, :group_points=>1000, :liders=>5},  #  Менеджер
          9  => {:points => 100, :group_points=>800,  :liders=>7},  #  Старший Менеджер
          10 => {:points => 100, :group_points=>600,  :liders=>10}, #  Директор
          11 => {:points => 100, :group_points=>400,  :liders=>15}, #  Старший Директор
          12 => {:points => 100, :group_points=>200,  :liders=>20}  #  Директор VIP  
        }
    end
         
    @rules = [] 
    rules.each do |status, req|
      @rules.push Rule.new(status, req)
    end
      
  end  
  
  def status_for(stats)
    status = 1
    @rules.each do |rule|
      status = rule.status if rule.match(stats) and rule.status > status 
    end
    status
  end
  
  def calc_pers_prem(status, points, total_points)
    return 0 if(total_points < 250)
    return 0.2*points if status>=5
    percents = {
      1 => 0,
      2 => 0.05,
      3 => 0.1, 
      4 => 0.15
    }
    percents[status] * points   
  end
  
  def calc_group_prem(status, group_points, total_points)
    return 0 if(total_points < 250)
    percents = {
      1 => { },
      2 => {1 => 0.05},
      3 => {1 => 0.1, 2 => 0.05},
      4 => {1 => 0.15, 2 => 0.10, 3 => 0.05},
      5 => {1 => 0.20, 2 => 0.15, 3 => 0.10, 4 => 0.05}    
    }
    perc = status > 5 ? percents[5] : percents[status]
    prem = 0
    group_points.each do |key, val|
      prem = prem + perc[key].to_f * val
    end
    prem  
  end
  
  def calc_lid_prem(status, lider_points)
    return 0 if(status < 6)
    percents = {
      6 => {1 => 0.09, 2 => 0.06},
      7 => {1 => 0.09, 2 => 0.06, 3 => 0.04},
      8 => {1 => 0.08, 2 => 0.06, 3 => 0.04, 4 => 0.02},
      9 => {1 => 0.08, 2 => 0.06, 3 => 0.04, 4 => 0.02,  5 => 0.02},
      10 => {1 => 0.08, 2 => 0.06, 3 => 0.04, 4 => 0.02,  5 => 0.02, 6 => 0.01},
      11 => {1 => 0.08, 2 => 0.06, 3 => 0.04, 4 => 0.02,  5 => 0.02, 6 => 0.01, 7 => 0.01},
      12 => {1 => 0.08, 2 => 0.06, 3 => 0.04, 4 => 0.02,  5 => 0.02, 6 => 0.01, 7 => 0.01}
    }
    perc = percents[status]
    prem = 0
    lider_points.each do |key, val|
      prem = prem + perc[key].to_f * val
    end
    prem
  end
        
end  

#--------------------------------------------------------------------
#  Класс расчёта статистики 
#--------------------------------------------------------------------
class StatsCalculator
  
  LIDER_STATUS = 5
  
  def initialize(db, options = {})
    @db = db
    @stats = {}
    @status_model = options[:status_model] || EasyStatusModel.new 
  end
  
  # инициализирует таблицу юзеров для заданного периода
  def build(month)
    @month = month
    @data = get_data_for_month(month)
    @stats = {}
  end
  
  # заполняет таблицу статистики показателями юзеров
  def refresh_stats
    calc_stats(:status) {|entry| entry[:status]}
    calc_stats(:points) {|entry| entry[:points]}
    calc_liders
    calc_group_points
    calc_lider_points
    calc_premium
    calc_status
        
  end
    
  # сохраняет рассчитанные показатели в базу 
  def save_stats
    return if @month == 0    
    @db.execute "BEGIN;"
    @stats.each do |id, entry|
      @db.execute("UPDATE `member` SET last_status = ? WHERE id=?", entry[:status], id)
      @db.execute("UPDATE `stats` SET group_points = ?, income = ?, pers_prem = ?, gr_prem = ?, lid_prem = ?, total_income = ?, status = ? WHERE member=? AND month=?", entry[:group_points_sum], entry[:income], entry[:pers_prem], entry[:group_prem], entry[:lid_prem], entry[:total_income], entry[:status], id, @month)
    end
    @db.execute "COMMIT;"  
  end
  
  # выводит итоги
  def printout
    print 'DONE'
  end
  
private

  # читает список юзеров из базы и создаёт каждой записи индекс подюзеров 
  def get_data_for_month(month)
    data = {}
    @db.execute("select * FROM stats join member on (member = member.id) where month = ?", month) do |row|
      data[row['member']] = {
        :member => row['member'],
        :parent => row['parent'],
        :points => row['points'],  
        :status => row['last_status'],
        :group_points => row['group_points'],  
        :childs => []  
      }
    end
    data.each do |key, entry|
      parent = entry[:parent]
      if data.has_key?(parent)  
        data[parent][:childs].push key          
      end         
    end
    data
  end
  
  # считает для каждого юзера функцию из блока и записывает результат в таблицу статистики под именем name  
  def calc_stats(name, &block)
    @data.each do |id, entry|
      @stats[id] = {} if not @stats.has_key?(id)
      @stats[id][name] = yield(entry, @stats[id])
    end
  end
   
  # расчитывает количество лидеров среди потомков   
  def calc_liders
    calc_stats :liders do |entry|
      entry[:childs].count{|id| @data.has_key?(id) and @data[id][:status]>=LIDER_STATUS}
    end
  end
  
  # расчитывает груповые очки
  def calc_group_points
    calc_stats :group_points do |entry|
      points = {entry[:status] => entry[:points]}
      entry[:childs].each do |id|
        merge_points(points, get_group_points(id)) 
      end
      points
    end
    @stats.each {|id, entry| entry[:group_points_sum] = entry[:group_points].values.inject(:+)}         
  end
  
  def merge_points(points, new_points)
    new_points.each do |status, value|
      points[status] = 0 if not points.has_key?(status) 
      points[status] += value
    end
  end
  
  def get_group_points(id)
    return {} if not @data.has_key?(id) or @data[id][:status] >= LIDER_STATUS
    entry = @data[id]
    points = {entry[:status] => entry[:points]}
    entry[:childs].each {|id| merge_points(points, get_group_points(id))} 
    points   
  end
  
  def calc_lider_points
    calc_stats :lider_points do |entry|
      get_lider_points(entry[:member])
    end  
  end
  
  def get_lider_points(id)
    return {} if not @data.has_key?(id) or @data[id][:status] < LIDER_STATUS
    stats = @stats[id]
    return stats[:lider_points] if(stats.has_key? :lider_points)  
    entry = @data[id]        
    points = {0 => stats[:group_points_sum]}
    entry[:childs].each {|cid| merge_lider_points(points, get_lider_points(cid))}
    stats[:lider_points] = points
  end
  
  def merge_lider_points(points, child_points)
    child_points.each do |gen, value|
      points[gen + 1] = 0 if not points.has_key?(gen + 1) 
      points[gen + 1] += value
    end
  end          
  
  def calc_status
    @stats.each do |id, stats|
      stats[:status] = @status_model.status_for(stats) 
    end  
  end
  
  def calc_premium
    @data.each do |id, entry|
      @stats[id] = {} if not @stats.has_key?(id)
      @stats[id][:income] = 0.43 * entry[:points]
      @stats[id][:pers_prem]  = @status_model.calc_pers_prem(entry[:status], entry[:points], @stats[id][:group_points_sum])
      @stats[id][:group_prem] = @status_model.calc_group_prem(entry[:status], @stats[id][:group_points], @stats[id][:group_points_sum])
      @stats[id][:lid_prem]   = @status_model.calc_lid_prem(entry[:status], @stats[id][:lider_points])  
      @stats[id][:total_income]  = @stats[id][:income] + @stats[id][:pers_prem] + @stats[id][:group_prem] + @stats[id][:lid_prem]
    end
  end
  
end
