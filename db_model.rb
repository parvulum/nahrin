require 'sqlite3'
require './logger.rb'
require './stats_calculator.rb'

#--------------------------------------------------------------------
class Db_model
  
  attr_accessor :month
  
  def initialize(db, options = {})
    @db = db
    @db.results_as_hash = true
    create
    @month = last_month
    @month_closed = false
    @logger = options[:logger] || ConsoleLogger.new
    @calculator = options[:calculator] || StatsCalculator.new(@db)
  end
  
  def on_start
    log :start, @month
  end  
  
  def last_month
    row = @db.get_first_row("SELECT month FROM settings WHERE id = 1") or return 1
    row['month'] || 1      
  end
  
  def set_month(month)
    @month = month
    @db.execute("INSERT OR REPLACE INTO settings VALUES (1, ?)", month)
  end
    
  def close_month
    return if @month_closed
    recalc(@month)
    log :month_closed, @month
  end
  
  def recalc(month = 0)
    month > 0 || month = @month
    @calculator.build(month)
    @calculator.refresh_stats
    @calculator.save_stats    
    @month_closed = true
  end   
  
  def next_month
    close_month if @month_closed == false
    set_month(@month + 1)
    @month_closed = false
    log :month_changed, @month
  end
  
  def step(mode = 'min', count = 1)
    count.to_i.times do
      @db.execute "BEGIN;"
      invite_for_member('all', mode)
      add_member_points('all', mode)
      @db.execute "COMMIT;"
      next_month
    end  
  end

  def create
    @db.execute "BEGIN;"
    @db.execute "CREATE TABLE IF NOT EXISTS settings (id INTEGER PRIMARY KEY, month INTEGER);"
    @db.execute "CREATE TABLE IF NOT EXISTS member (id INTEGER PRIMARY KEY, name TEXT, parent INTEGER, last_status INTEGER DEFAULT 1, date INTEGER);"
    @db.execute "CREATE INDEX IF NOT EXISTS parent_idx ON member (parent);"
    @db.execute "CREATE TABLE IF NOT EXISTS stats (member INTEGER, month INTEGER, status INTEGER DEFAULT 1, points INTEGER DEFAULT 0, group_points INTEGER DEFAULT 0, income INTEGER DEFAULT 0, pers_prem INTEGER DEFAULT 0, gr_prem INTEGER DEFAULT 0, lid_prem INTEGER DEFAULT 0, total_income INTEGER DEFAULT 0, siblings INTEGER DEFAULT 0);"
    @db.execute "CREATE UNIQUE INDEX IF NOT EXISTS member_month_idx ON stats (member, month);"
    @db.execute "COMMIT;"
  end
  
  def clear
    @db.execute "BEGIN;"
    @db.execute "DELETE FROM member;"
    @db.execute "DELETE FROM stats;"
    @db.execute "COMMIT;"
    set_month(1)
    @month_closed = false   
    log :clear
  end
  
  def log(message, *args) 
    if(@logger.respond_to? message)
      @logger.send(message, *args)
    else
      @logger.log_message(message)   
    end
  end
  
  def add_members(number = 1)
    number.to_i.times do|x|
      @db.execute("INSERT INTO member (name, parent, date) VALUES (?, ?, ?)", member_name, 0, @month)
      user = find @db.last_insert_row_id
      log :user_created, user
    end
  end
  
  def add_member_points(member, points)
    if(member == 'all')
      members_of(@month).each {|mem| add_member_points(mem['id'], points) }
    end  
    user = find(member) or return    
    stats = stats(member)
    
    points = min_points(user) if(points == 'min')
    points = rand(500)+100    if(points == 'rand')
    points = [points.to_i, 0].max   
    if stats
      @db.execute("UPDATE `stats` SET points=points + ? WHERE member=? AND month=?", points, member, @month)
    else
      @db.execute("INSERT INTO stats (member, month, points) VALUES (?, ?, ?)", member, @month, points)
    end
    log :add_points, user, points
  end
  
  def find(member_id) 
    @db.get_first_row("SELECT * FROM member WHERE id = ?", member_id)
  end
  
  def stats(member_id)
    @db.get_first_row("SELECT * FROM stats WHERE member = ? AND month = ?", member_id, @month)
  end
  
  def invite_for_member(parent, count = 1)
    if(parent == 'all')
      members_of(@month).each {|mem| invite_for_member(mem['id'], count) }
      return  
    end
    user = find(parent) or return
    
    count = min_invites_count(user) if count == 'min'
    count = rand(4) if count == 'rand'
    
    count.to_i.times do     
      name = member_name
      @db.execute("INSERT INTO member (name, parent, date) VALUES (?, ?, ?)", name, parent, @month)
      new_user = find @db.last_insert_row_id
      log :invites, user, new_user
    end
  end
  
  def min_invites_count(member)
    age = @month - member['date']
    age.odd? ? 1 : 2
  end
  
  def min_points(member)
    @month == member['date'] ? 60 : 100;
  end    
  
  def each_member(&closure)
    list = @db.execute("select * FROM member") 
    list.each do |row|
      yield row
    end
  end
    
  def minimal_step()
    each_member do |member|
      age = @month - member['date']
      num = age.odd? ? 1 : 2
      num.times { invite_for_member(member['id']) }
    end
    each_member do |member|
      points = member['date'] == @month ? 60 : 100
      add_member_points(member['id'], points) 
    end
  end
  
  def print_list
    @db.execute("select * FROM member") do |row|
      p row
    end
  end
  
  def print_stats(month = 0)
    month.to_i > 0 || month = (@month_closed ? @month : [@month - 1, 1].max)
    if month.to_i > @month
      puts 'No information for this month'
      return 
    end    
    puts "Month: #{month}"
    format = '%-3s %-14s %-2s %-6s %-6s %-8s %-8s %-8s %-8s %-8s' 
    puts format % ['ID', 'Member name', 'St', 'Points', '43%', 'GrPoints', 'PersPrem', 'GrPrem' ,'LidPrem', 'Income']
    puts '-' * 80
    floats_format = '%-3s %-14s %-2s %-6s %-6.1f %-8.1f %-8.1f %-8.1f %-8.1f %-8.1f'      
    members_of(month).each do |user|
      status = status_str(user['status'] || user['last_status']) 
      puts floats_format % [user['id'], user['name'], status, user['points'].to_i, user['income'].to_f, user['group_points'].to_f, user['pers_prem'].to_f, user['gr_prem'].to_f, user['lid_prem'].to_f, user['total_income'].to_f] 
    end
  end 
  
  def members_of(month)
    @db.execute("SELECT * FROM member LEFT JOIN stats ON (stats.member = member.id AND stats.month = ?) WHERE member.date <= ?", month.to_i, month.to_i)
  end
  
  def stats_for(id)
    @db.execute("SELECT * FROM stats WHERE member = ? ORDER BY month", id)
  end
  
  def print_info(month = @month)
    month || month = @month
    puts "Month: #{month}"
    puts '%s %-30s %-6s %-6s' % ['ID', 'Member name', 'Status', 'Points']
    puts '-' * 50      
    members_of(month).each do |user|
      puts '%i. %-30s %-6s %-6i' % [user['id'], user['name'], status_str(user['status'] || user['last_status']), user['points'].to_i] 
    end
  end
  
  def print_userinfo(id = 1)
    user = find(id)
    if(not user)
      puts 'User not found'
      return 
    end  
    puts "#{user['name']} (#{id})"
    format = '%-6s %-3s %-8s %-8s %-8s %-8s %-8s %-8s' 
    puts format % ['Month', 'St', 'Points', 'GrPoints', 'PersPrem', 'GrPrem' ,'LidPrem', 'Income']
    puts '-' * 80
    stats_for(id).each do |stats|
      puts '%-6s %-3s %-8.1f %-8.1f %-8.1f %-8.1f %-8.1f %-8.1f' % 
        [stats['month'].to_i, status_str(stats['status']), stats['points'].to_f, stats['group_points'].to_f, 
         stats['pers_prem'].to_f, stats['gr_prem'].to_f, stats['lid_prem'].to_f, stats['total_income'].to_f]
    end  
  end  
  
  def status_str(status)
    stat = {
      1  => 'C',  
      2  => 'SC',
      3  => 'I',
      4  => 'SI',
      5  => 'L', 
      6  => 'SL',
      7  => 'ML',
      8  => 'M', 
      9  => 'SM',
      10 => 'D', 
      11 => 'SD',
      12 => 'VIP'  
    }
    stat[status.to_i]   
  end   
  
  def rand_str()
    vowels = ['a','e','i','o','u','y'] 
    consonant = ['b','c','d','f','g','h','j','k','l','m','n','p','q','r','s','t','v','w','x','z']
    name = ''
    rand(1..3).times do
      name = name + consonant.sample + vowels.sample 
    end
    name.capitalize!
  end
  
  def member_name()
    rand_str + ' ' + rand_str
  end
  
end