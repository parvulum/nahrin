#--------------------------------------------------------------------
#  Логер в консоль  
#--------------------------------------------------------------------
class ConsoleLogger

  def log_message(message)
    puts message.to_s
  end
  
  def clear
    log_message "Database cleaned up"
  end
  
  def start(month)
    log_message "Started on month #{month}"
  end
  
  def user_created(user)
    log_message "User #{user['name']} created with id:#{user['id']}"
  end
  
  def add_points(user, points)
    log_message "#{user['name']} brings #{points} points"
  end
  
  def invites(user, new_user)
    log_message "#{user['name']} invites #{new_user['name']}"
  end
  
  def month_closed(month)
    log_message "Month #{month} is closed"
  end
  
  def month_changed(month)
    log_message "Month changed to #{month}"
  end
  
end