

class NahrinEngine
  
  def initialize(options)
    @model = options[:model]
  end
  
  def exit
    puts 'Good bye!'
    Kernel.exit
  end
  
  def info(*args)
    @model.print_info *args[0]
  end
  
  def put(*args)
    @model.send(:add_member_points, *args[0..1])
  end
  
  def invite(*args)
    @model.send(:invite_for_member, *args[0..1])
  end
  
  def intro
    puts 'Swiss Nahrin MLM Simulator'
    puts 'by Margarita Rizhova, 2012'
    puts 'type \'help\' to read instructions or \'exit\' to exit'
    @model.on_start
  end
  
  def help
    puts 'Available commands:'
    puts 'exit - to leave this programm'
    puts 'clear - clean up the database and reset a model to initial state'
    puts 'add [#number] - add first members'
    puts 'put <#id|all> <#points|min|rand> - add points to member with id'
    puts 'invite <#id|all> <#number|min|rand> - add a number of new members invited by member with id'
    puts 'info [#month] - print a list of users with personal points for month'
    puts 'close_month - refresh statistics and member statuses for current month'
    puts 'stats [#month] - print information about all members for month: statuses, points, premiums, incomes'
    puts 'next - close current month and starts the next one'
    puts 'userinfo <#id> - print annual statistics for member with id'
    puts 'step <min|rand> [#number] - make a number of automatical steps, minimal or random'
  end
  
  def members
    @model.print_list
  end
  
  def stats(*args)
    @model.send(:print_stats, *args[0])
  end
  
  def userinfo(*args)
    @model.send(:print_userinfo, *args[0])
  end
  
  
  def add(*args)
    @model.send(:add_members, *args[0])  
  end  
  
  def close_month
    @model.send(:close_month)
  end
  
  def next
    @model.send(:next_month)            
  end  
  
  def step(*args)
    @model.send(:step, *args[0..1])    
  end
  
  def recalc(*args)
    @model.send(:recalc, *args[0])
  end
  
  def clear
    @model.clear  
  end
  
end
