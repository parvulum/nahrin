#encoding: utf-8

require 'optparse'
require './db_model.rb'
require './engine.rb'

options = {}

opt_parser = OptionParser.new do |opt|
  opt.banner = "Usage: Nahrin [OPTIONS]"
  opt.separator  ""
  opt.separator  "Options"  
  opt.on('-d', '--database FILE', 'Database filename') do|file|
    options[:filename] = file
  end
  
end

begin
  opt_parser.parse!
rescue Exception=>e
  puts 'Error: ' + e.message
  puts opt_parser.help
  exit
end

options[:filename] = 'nahrin.db' if not options[:filename]
  
db = SQLite3::Database.new options[:filename]
model = Db_model.new(db, 
  {
    :logger => ConsoleLogger.new,
    :calculator => StatsCalculator.new(db),
  }
)

model.create()
engine = NahrinEngine.new({:model=>model})

prog = [
  'intro'
]
  
while true do
  prog.push(gets || '') if prog.empty?
  args = prog.shift.strip.split(/\s+/)
  cmd = args.shift
  if cmd
    if engine.respond_to?(cmd)
      engine.send(cmd, *args)
    else
      print "Unknown command '#{cmd}'!\n"  
    end
  end      
end
